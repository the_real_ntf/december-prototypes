$(document).ready(function() {
    //Check if the user is rejoining
    //ps: This value is set by Express if browser session is still valid
    var user = $('#user').text();
    // show join box
    if (user === "") {
        $('#ask').show();
        $('#ask input').focus();
    } else { //rejoin using old session
        join(user);
    }

    // join on enter
    $('#ask input').keydown(function(event) {
        if (event.keyCode == 13) {
            $('#ask a').click();
        }
    });

    /*
     When the user joins, hide the join-field, display chat-widget and also call 'join' function that
     initializes Socket.io and the entire app.
     */
    $('#ask a').click(function() {
        var name = $('#ask input').val();
        console.log("Joining with name " + name);
        join(name);
    });


    function initSocketIO(name) {

        var host = window.location.host; //.split(':')[0];
     /*   var socket = io.connect('http://' + host, {
            reconnect: false,
            'try multiple transports': false
        });
        */
        var ws = new WebSocket('ws://' + host);
        var intervalID;
        var reconnectCount = 0;
        var container = $('div#msgs');

        ws.onopen = function() {
            console.log('connecting');
            // send join message
            var jsonMsg = JSON.stringify({
                action: 'join',
                msg: name
            });
            //console.log("sending " + jsonMsg);
            ws.send(jsonMsg);
        };
        ws.onclose = function() {
            console.log('disconnect');
            intervalID = setInterval(tryReconnect, 4000);
        };
        ws.onerror = function(err) {
            console.log('error: ' + err);
        };
       
        /*
         When a message comes from the server, format, colorize it etc. and display in the chat widget
         */
        ws.onmessage = function(msg) {
            //msg.msg
            //onsole.log("Received message: " + msg.data);
            var message = JSON.parse(msg.data);

            var action = message.action;
            var struct = container.find('li.' + action + ':first');

            if (struct.length < 1) {
                console.log("Could not handle: " + message);
                return;
            }

            // get a new message view from struct template
            var messageView = struct.clone();

            // set time
            messageView.find('.time').text((new Date()).toString("HH:mm:ss"));

            switch (action) {
                case 'message':
                    var matches;
                    // someone starts chat with /me ...
                    if (matches = message.msg.match(/^\s*[\/\\]me\s(.*)/)) {
                        messageView.find('.user').text(message.user + ' ' + matches[1]);
                        messageView.find('.user').css('font-weight', 'bold');
                        // normal chat message
                    } else {
                        messageView.find('.user').text(message.user);
                        messageView.find('.message').text(': ' + message.msg);
                    }
                    break;
                case 'join':

                case 'control':
                    messageView.find('.user').text(message.user);
                    messageView.find('.message').text(message.msg);
                    messageView.addClass('control');
                    break;
            }

            // color own user:
            if (message.user == name) messageView.find('.user').addClass('self');

            // append to container and scroll
            container.find('ul').append(messageView.show());
            container.scrollTop(container.find('ul').innerHeight());
        };

        /*
         When the user creates a new chat message, send it to server via socket.emit w/ 'chat' event/channel name
         */
        $('#channel form').submit(function(event) {
            event.preventDefault();
            var input = $(this).find(':input');
            var msg = input.val();
            //console.log("Sending msg: " + msg);
            var jsonMsg = JSON.stringify({
                action: 'message',
                msg: msg
            });
            //console.log("Sending json: " + jsonMsg);
            ws.send(jsonMsg);
            input.val('');
        });


    }


    function join(name) {
        $('#ask').hide();
        $('#channel').show();
        $('input#message').focus();
        //console.log("Starting as " + name);
        initSocketIO(name);
    }
});
