'use strict';
var redis = require('redis');
var sub = redis.createClient();
var pub = redis.createClient();
sub.subscribe('chat');

module.exports = function (wsio) {
    wsio.on('request', function (request) {
        var connection = request.accept(null, request.origin);
        var userName = false;
        //console.log ("Request key: " + request.key);
        //console.log (request);
        connection.on('message', function (data) {
            var reply = "";
            //console.log(data);
            try {
                var msg = JSON.parse(data.utf8Data);
                //console.log("Processing a " + msg.action);
                switch (msg.action) {
                    case 'message':
                        reply = JSON.stringify({
                            action: 'message',
                            user: userName,
                            msg: msg.msg
                        });
                        break;
                    case 'join':
                        //console.log("Received a new join request from " + msg.msg);
                        userName = msg.msg;
                        reply = JSON.stringify({
                            action: 'join',
                            user: msg.msg,
                            msg: msg.msg + " has joined the chat"
                        });
                        break;
                    case 'control':
                        break;
                }
                //console.log("Publishing " + reply);
            } catch (e) {
                console.log(e);
            }
            pub.publish('chat', reply);
        });
        connection.on('close', function (connection) {
            console.log('WebSocketServer.on.close', connection);
        });
        connection.on('error', function (err) {
            console.log('WebSocketServer.on.error', err);
        });
        sub.on('message', function (channel, message) {
            console.log("Sub received message " + message);
            connection.sendUTF(message);
        });
    });

}
